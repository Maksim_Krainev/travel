const nav = document.querySelector('#nav')
const navBtn = document.querySelector('#nav-btn')
const navBtnImg = document.querySelector('#nav-btn-img')

navBtn.onclick = () => {
	if (nav.classList.toggle('open')){
		navBtnImg.src = "img/svg/burger-close.svg";
	} else{
		navBtnImg.src = "img/svg/burger-open.svg";
	}
}

AOS.init();